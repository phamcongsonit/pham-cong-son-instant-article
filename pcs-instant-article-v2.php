<?php



function filter_content_tag( $content ) {
	// $content = preg_replace("/\t+/", " ", $content);
	// If this is a revision, don't send the email.
	$content = preg_replace('/\<[\/]{0,1}div[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}article[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}section[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}iframe[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}a[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}u[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}font[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}link[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}meta[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}video[^\>]*\>/i', '', $content);
	
	$content = preg_replace('/\<[\/]{0,1}header[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}figure[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}source[^\>]*\>/i', '', $content);
	$content = preg_replace('/<time\b[^>]*>(.*?)<\/time>/i', '', $content);
	$content = preg_replace('/<address\b[^>]*>(.*?)<\/address>/i', '', $content);
	

	$scriptA = strpos($content, '<script>');
	$scriptB = strpos($content, '</script>');
	$contentA = substr($content, 0, $scriptA);
	$contentB = substr($content, $scriptB + 9, strlen($scriptB));
	$content = $contentA.$contentB;

	return $content;
}
add_filter( 'content_save_pre', 'filter_content_tag', 10, 3 );
