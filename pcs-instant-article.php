<?php
/**
 * Plugin Name: Pham Cong Son Instant Article
 * Description: Remove special tag to Instant Article
 * Version: 2.1
 * Author: Pham Cong Son
 * Author URI: http://phamcongson.com
 * Bitbucket Plugin URI: https://bitbucket.org/phamcongsonit/pham-cong-son-instant-article
 */



function filter_content_tag( $content ) {
	// $content = preg_replace('!\s+!', ' ', $content);

	$content = preg_replace('/\<[\/]{0,1}div[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}article[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}section[^\>]*\>/i', '', $content);
	//$content = preg_replace('/\<[\/]{0,1}iframe[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}a[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}header[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}u[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}font[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}li[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}figure[^\>]*\>/i', '', $content);
	$content = preg_replace('/\<[\/]{0,1}figcaption[^\>]*\>/i', '', $content);
	# $content = preg_replace('/\<[\/]{0,1}figcaption[^\>]*\>/i', '', $content);
	// $content = preg_replace('/<figure\b[^>]*>(.*?)<\/figure>/i', '', $content);
	$content = preg_replace('/<figcaption\b[^>]*>(.*?)<\/figcaption>/i', '', $content);
	$content = preg_replace('/<video\b[^>]*>(.*?)<\/video>/i', '', $content);
	
	$content = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $content);
	$content = preg_replace('/(<[^>]+) class=".*?"/i', '$1', $content);
	$content = str_replace("<hr/>", "", $content);
	$content = str_replace("<hr />", "", $content);
	$content = str_replace("<hr>", "", $content);

	$scriptA = strpos($content, '<video');
	$scriptB = strpos($content, '</video>');
	if ($scriptA != false && $scriptB != false){
		$contentA = substr($content, 0, $scriptA);
		$contentB = substr($content, $scriptA + 8, strlen($scriptB));
		$content = $contentA.$contentB;
		var_dump($content);
		exit;
	}
	

	return $content;
}
add_filter( 'content_save_pre', 'filter_content_tag', 10, 3 );
